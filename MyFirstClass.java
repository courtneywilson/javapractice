public class MyFirstClass{

    public static void main(String[] args) {
    
    String model = "Chevy";
    String make = "Equinox";

    double engineSize = 1.2;
    byte gear = 2;

    System.out.println(
        "The make is " + make
    );
    System.out.println(
        "The model is " + model
    );

    System.out.println(
        "The engine size is " + engineSize
    );

    short speed = (short) (gear * 20);
    System.out.println("The speed is " + speed);

    if(engineSize <= 1.3){
        System.out.println("Engine is weak");
    }
    else
    {
        System.out.println("Engine is Strong ");
    }

    Account[] arrayOfAccounts = new Account[5];

    double[] amounts = {23,5444,2,345,34};
    String[] names = {"Picard","Ryer","Worf","Troy","Data"};

    int count;
    for (count =0; count < arrayOfAccounts.length; count++) {

        arrayOfAccounts[count] = new Account(names[count], amounts[count]);
        System.out.println("Values Before Interest");
        System.out.println(arrayOfAccounts[count].getName() + " " + arrayOfAccounts[count].getBalance());

    }



   

}

}
