public class TestAccount2 {

    public static void main(String[] args) {
    
      Account[] arrayOfAccounts = new Account[5];

      double[] amounts = {23,5444,2,345,34};
      String[] names = {"Picard","Ryer","Worf","Troy","Data"};

    int count;
    for (count =0; count < arrayOfAccounts.length; count++) {

        arrayOfAccounts[count] = new Account(names[count], amounts[count]);
        //System.out.println("Values Before Interest");
       // System.out.println(arrayOfAccounts[count].getName() + " " + arrayOfAccounts[count].getBalance());

        arrayOfAccounts[count].addInterest();
        //System.out.println("Values After Interest");
        //System.out.println(arrayOfAccounts[count].getBalance());      
    }

    arrayOfAccounts[1].withdraw();
    arrayOfAccounts[1].withdraw(10000);

    arrayOfAccounts[4].withdraw(500);
    arrayOfAccounts[4].withdraw();
    
  }

}