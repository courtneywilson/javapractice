public class Account {

    private String name;
    private double balance;
    private static double interestRate = 0.2;
    

    public Account(String n, double b){

        name = n;
        balance = b;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void addInterest(){
        
        balance = balance + (balance*interestRate);
        setBalance(balance);

    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public boolean withdraw(double amount){

        if(amount <= balance){

            balance = balance - amount; 
            System.out.println(amount + " was withdrawn. New Balnace is: " + balance);

            return true;
        }
        System.out.println("Not Enough Funds :(");
        return false;
    }    

    public boolean withdraw(){

        boolean withdrawnFunds = withdraw(20);
        return withdrawnFunds;

    }
}